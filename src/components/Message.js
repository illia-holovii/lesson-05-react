import React from 'react';
import './Message.css';

class Message extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const data = this.props.data;

        return (
            <div className="Message">
                <img src={data.avatar} />
                <div className="MessageContent">
                    <p>{data.message}</p>
                    <div className="MessageInfo">
                        <span>{data.created_at}</span>
                        <span>&#10084; 2</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default Message;