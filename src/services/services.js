import { callApi } from './api';

class FighterService {
  async getFighters() {
    try {
      const apiResult = await callApi('GET');
  
      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}
  
export const fighterService = new FighterService();